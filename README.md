![alt text](./images/jp-books.jpg "jp-books")
## HOW TO 日本語 (にほんご)  
This is a collection of Japanese language usage example, grammar rules, and proper usage tips that I want to remember. The main criterion for inclusion here is whether I thought it would help me. I indulge in the, probably vain, idea that this collection will resonate with others seeking to read, write, and understand 				Japanese. A note: it's assumed that you've already familiarized yourself with hiragana and katakana. If you're still learning, or need a refresher, here's a [page you can consult](./modules/kana.md). If you want something interactive, here's the code for a Python program I wrote [graphical Python program](https://gitlab.com/sylvanix/python_pieces/-/blob/master/code/tkinter_example/kana.py). It's ok if you don't yet know any kanji though; that's one of the things you'll learn here.  
  

### Common Phrases

##### Before/After a meal
頂きます (itadakimasu) - roughly meaning "let's eat", "I will eat", or "thanks for the food". Used before beginning a meal. When eating with others, you should wait until everyone is ready and say the phrase together.  

The kun reading of ![alt text](./images/i_ta_da.png "i_ta_da") is いただ  


ご馳走様でした (gochisōsamadeshita) - said after the meal in appreciation. You can use the short form, “gochisōsama”, in more casual settings.  

The combination of ![alt text](./images/ha.png "ha")![alt text](./images/ha_shi.png "ha_shi")![alt text](./images/sa_ma.png "sa_ma")![alt text](./images/o_n.png "o_n") is pronounced "gochisōsama".  

##### Leaving/Returning home
行ってきます (ittekimasu) is said by someone leaving home.
行ってらっしゃい (itterasshai) is said to the person leaving.

只今 (tadaima) is said by a person who is just stepping in the door upon returning home.
お帰りなさい (okaerinasai, or "okaeri" for short) - is the welcome said by those in the home to greet the returning person		 who has just said 只今.

##### Introducing yourself.
"Hello, my name is David. Pleased to meet you."  
どうも。初めまして。デビッド です。よろしく。   
The kun reading of ![alt text](./images/ha_ji.png "ha_ji") is はじ  

よろしく, meaning something like "pleased to meet you", is often represented in chat as 4649. The first 4 is pronounced "yo", the 6 "ro", the second 4 "shi" and the 9 "ku". These readings come from the way the numbers are pronounced in Japanese. 4 can be "shi" or "yon", 6 is "roku" and 9 is "kyuu" or "ku".  

To be slightly more polite, say よろしくお願いします instead of just よろしく, like this:
どうも。初めまして。デビッド です。よろしくお願いします。  

##### Answering yes / no
In textbooks for English speakers one is often taught to use はい for "yes" and いいえ for "no", but according to [Hitoki](https://www.youtube.com/c/Onomappu), though はい is used quite often, いいえ is only used for yes/no options in video games or in surveys, not in the spoken language. To answer no politely, use いえ. If among peers, then use いや.  

##### Native Japanese don't often use こんにちは
こんにちは ("konnichiwa") is rarely used in any situation. Instead employ these greetings:

* よ ("yo") - informal, like between friends
* う-っす ("uusu") - informal, like between friends
* おぉ ~ ! ("ooh") - very casual
* 元気 ("genki")
* 待った? ("matta?") - did I make you wait?
* お疲れ様です (おつかれさまです "otsukare sama desu") - formal; can be used, for example, to greet one's boss and co-workers.
* いつもお世話になっております (いつもおせわになっております "itsumo osewa ni natte orimasu") - formal, for greeting people outside your group.

A good discussion of this can be found [in this video](https://www.youtube.com/watch?v=0FvHIG4SWSA).

##### Native Japanese don't often use さようなら  
Perhaps because さようなら ("sayounara") is thought of as too cold or distant due to its use in popular songs to say goodbye to a lover after a breakup, Japanese don't use it, excepting maybe grade-school pupils to bid their teacher goodbye for the day. Instead say the following: 
 
* じゃあまたね ("jaa matane") - like "see you later" to a friend
* じゃあね ("jaa ne") - a shortened version of the phrase above
* 元気でね ("genki dene") - to friends we don't see often or acquaintances
* お疲れ様です ("otsukare sama desu") - is used in more formal situations, like to a work colleague. In fact, お疲れ様です is used as a greeting too.
* ではまた ("dewa mata") is a bit more informal than お疲れ様です, but a bit more formal than またね.
* 失礼します ("shitsurei shimasu") and 失礼いたします ("shitsurei itashimasu") - these formal goodbyes are used, for example, by a shop employee to a customer.

A good discussion of this can be found [in this video](https://www.youtube.com/watch?v=Rc0V3Mhz8zU).

##### Are you alright? (everthing ok?)  
大丈夫？ (だいじょうぶ ?)



##### When to use ください (kudasai) or お願します (onegaishimasu) in requests?  
xxx	


### My favorite resources

1. [Takoboto](https://takoboto.jp)  
2. [Japanese Ammo with Misa](https://www.youtube.com/c/JapaneseAmmowithMisa)  
3. [Kiku-Nihongo Listening and Learning Japanese](https://www.youtube.com/channel/UCadUNjnWpKZ9SJ4uuSr1y9w)  
4. [NHK News (easy)](https://www3.nhk.or.jp/news/easy)  
5. http://hukumusume.com/douwa/index.html
6. [Google Translate](https://translate.google.com/?hl=en&tab=TT&sl=en&tl=ja&op=translate)  
7. http://www.rikai.com/library/kanjitables/kanji_codes.unicode.shtml
8. https://japanese.stackexchange.com/


